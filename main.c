/******************************************************************************
* Software License Agreement
*
* Copyright (c) 2016, Infineon Technologies AG
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* Neither the name of the copyright holders nor the names of its contributors
* may be used to endorse or promote products derived from this software
* without specific prior written permission

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY,OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
* To improve the quality of the software, users are encouraged to share
* modifications, enhancements or bug fixes with Infineon Technologies AG
* (dave@infineon.com).
*
*****************************************************************************/

/**
 * @file
 * @date 28 Jul, 2016
 * @version 1.0.1
 *
 * @brief XMC4700/4800 Relax Kit USB virtual COM demo example based on Apps
 *
 * The USB example initializes a VCOM USB device. After connection to host is established,
 * every received byte is returned to the host.
 *
 * History <br>
 *
 * Version 1.0.0 Initial <br>
 * Version 1.0.1 minor code restructuring to fit coding guidelines <br>
 * minor changes on comments to fit coding guidelines <br>
 * renaming of project file <br>
 *
 */

/****************************************************************
* HEADER FILES
***************************************************************/
#include <DAVE.h>

/****************************************************************
* MACROS AND DEFINES
***************************************************************/

/****************************************************************
* PROTOTYPES
***************************************************************/

/****************************************************************
* LOCAL DATA
***************************************************************/
int8_t rx_buffer[64] = { 0 };
const int8_t *received = (int8_t *)"\nReceived: ";

/****************************************************************
* API IMPLEMENTATION
***************************************************************/
int main(void)
{
  uint16_t bytes = 0;

  /*Initialize Dave (including USB) */
  if(DAVE_Init() == DAVE_STATUS_FAILURE)
  {
    /* Placeholder for error handler code.*/
    XMC_DEBUG(("DAVE APPs initialization failed\n"));
    while(1U)
    {
        /* do nothing */
    }
  }

  /* Virtual USB COM device to USB host */
  if (USBD_VCOM_Connect() != USBD_VCOM_STATUS_SUCCESS)
  {
    return -1;
  }
  /* Wait until enumeration is finished */
  while (!USBD_VCOM_IsEnumDone());

  while(1U)
  {
    /* Check the number of byes received from host */
    bytes = USBD_VCOM_BytesReceived();
    if (bytes)
    {
      // Get all bytes received
      for (int i = 0; i < bytes && i < sizeof(rx_buffer); ++i) {
        USBD_VCOM_ReceiveByte(rx_buffer + i);
      }

      USBD_VCOM_SendString(received);

      /* Return bytes received to host */
      USBD_VCOM_SendData(rx_buffer, bytes);
    }
    /* Call general managment task */
    CDC_Device_USBTask(&USBD_VCOM_cdc_interface);
  }
  return 1;
}
